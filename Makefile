PROJECT_NAME := notify

build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ${PROJECT_NAME} cmd/main.go

version:
	$(eval VERSION=$(shell ./version.sh))

tag: version
	$(eval LATEST_TAG=v${VERSION})
	git tag -a v${LATEST_TAG} -m v${LATEST_TAG}

image: version
	docker build -f docker/Dockerfile -t ${REGISTRY}/${PROJECT_NAME}:${VERSION} .

deploy_image: image
	docker push ${REGISTRY}/${PROJECT_NAME}:${VERSION}

deploy: tag deploy_image
	git push origin ${LATEST_TAG}
	docker tag ${REGISTRY}/${PROJECT_NAME}:${VERSION} ${REGISTRY}/${PROJECT_NAME}:latest
	docker push ${REGISTRY}/${PROJECT_NAME}:latest

.PHONY: image build