# notify
notify gives users access to simplified system events that they may
be interested in.

## Hey, an event.
Different parts of the system publish messages explaining the work
they have done. Their goal should be to describe events with complete
information. However, users will likely not need access to most of
these events, and the events they can access will not be user friendly.

The notification system is responsible for sanitizing published events
into messages that a user could view in their entirety. It then takes
appropriate steps to ensure the user will have access to the information.

## Stuff goes somewhere.
Notifications are sent to two parts of one destination. In Redis, each
user will have a sorted set of notifications. A Unix timestamp is the
score and the element is a JSON notification. That same element is
also published to a channel for the user.

## Notification Structure

| field     | type    | description                   |
|:---------:|:-------:|:-----------------------------:|
| timestamp | int64   | a Unix timestamp              |
| type      | int     | the category of notification  |
| title     | string  | a brief description           |
| message   | string  | the full notification content |

## Notification Types

| value | description                               |
|:-----:|:-----------------------------------------:|
| 1     | indicates the result of building a module |

## Redis Names
- Live Notifications
  - Notifications are published to a channel for the user, even if they are
    not online. The channel key is `notification_stream:<username>`.
- Persistent Notifications
  - All notifications are sent to a sorted set, even if a user received the
    published data. The set key is `notifications:<username>` and the score
    is a Unix timestamp.

## Environment Variables
- REDIS_HOST
- REDIS_PORT
- KAFKA_BROKERS