package main

import (
	"os"
	"os/signal"
	"strings"
	"syscall"

	"gitlab.com/botbin/notify/pkg/message"
	"gitlab.com/botbin/notify/pkg/pipe"
	"go.uber.org/zap"
)

func init() {
	l, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	zap.ReplaceGlobals(l)
}

const version = "0.4.0"

func main() {
	zap.S().Infof("notify v%s", version)

	publisher, err := message.NewPublisher()
	if err != nil {
		zap.S().Panicw("failed to configure publisher", "error", err)
	}

	coordinator := pipe.NewCoordinator()
	coordinator.Use(publisher)

	brokers := strings.Split(os.Getenv("KAFKA_BROKERS"), ",")
	coordinator.Accept(pipe.NewBuildStatusHandler(brokers))

	wireGracefulExit(coordinator)
	coordinator.Start()
}

// wireGracefulExit ensures that communication ends before shutting down.
func wireGracefulExit(coordinator pipe.Coordinator) {
	exit := make(chan os.Signal)
	signal.Notify(exit, syscall.SIGTERM)
	signal.Notify(exit, syscall.SIGINT)

	go func() {
		<-exit
		zap.S().Infow("accepted shutdown signal...")

		zap.S().Infow("stopping coordinator...")
		coordinator.Stop()
		zap.S().Infow("coordinator stopped")

		zap.S().Infow("exiting")
		os.Exit(0)
	}()
}
