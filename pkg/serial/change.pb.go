// Code generated by protoc-gen-go. DO NOT EDIT.
// source: gitlab.com/botbin/module_service/schema/change.proto

/*
Package serial is a generated protocol buffer package.

It is generated from these files:
	gitlab.com/botbin/module_service/schema/change.proto
	gitlab.com/botbin/module_service/schema/request.proto
	gitlab.com/botbin/module_service/schema/specification.proto

It has these top-level messages:
	StateChange
	Creation
	Deletion
	RootDeletion
	Request
	Module
	Definition
	Metadata
	Spec
	Location
*/
package serial

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// StateChange models an event that should alter the state of services
// that care about modules.
type StateChange struct {
	// Types that are valid to be assigned to Event:
	//	*StateChange_Creation
	//	*StateChange_Deletion
	//	*StateChange_RootDeletion
	Event isStateChange_Event `protobuf_oneof:"event"`
}

func (m *StateChange) Reset()                    { *m = StateChange{} }
func (m *StateChange) String() string            { return proto.CompactTextString(m) }
func (*StateChange) ProtoMessage()               {}
func (*StateChange) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

type isStateChange_Event interface {
	isStateChange_Event()
}

type StateChange_Creation struct {
	Creation *Creation `protobuf:"bytes,1,opt,name=creation,oneof"`
}
type StateChange_Deletion struct {
	Deletion *Deletion `protobuf:"bytes,2,opt,name=deletion,oneof"`
}
type StateChange_RootDeletion struct {
	RootDeletion *RootDeletion `protobuf:"bytes,3,opt,name=root_deletion,json=rootDeletion,oneof"`
}

func (*StateChange_Creation) isStateChange_Event()     {}
func (*StateChange_Deletion) isStateChange_Event()     {}
func (*StateChange_RootDeletion) isStateChange_Event() {}

func (m *StateChange) GetEvent() isStateChange_Event {
	if m != nil {
		return m.Event
	}
	return nil
}

func (m *StateChange) GetCreation() *Creation {
	if x, ok := m.GetEvent().(*StateChange_Creation); ok {
		return x.Creation
	}
	return nil
}

func (m *StateChange) GetDeletion() *Deletion {
	if x, ok := m.GetEvent().(*StateChange_Deletion); ok {
		return x.Deletion
	}
	return nil
}

func (m *StateChange) GetRootDeletion() *RootDeletion {
	if x, ok := m.GetEvent().(*StateChange_RootDeletion); ok {
		return x.RootDeletion
	}
	return nil
}

// XXX_OneofFuncs is for the internal use of the proto package.
func (*StateChange) XXX_OneofFuncs() (func(msg proto.Message, b *proto.Buffer) error, func(msg proto.Message, tag, wire int, b *proto.Buffer) (bool, error), func(msg proto.Message) (n int), []interface{}) {
	return _StateChange_OneofMarshaler, _StateChange_OneofUnmarshaler, _StateChange_OneofSizer, []interface{}{
		(*StateChange_Creation)(nil),
		(*StateChange_Deletion)(nil),
		(*StateChange_RootDeletion)(nil),
	}
}

func _StateChange_OneofMarshaler(msg proto.Message, b *proto.Buffer) error {
	m := msg.(*StateChange)
	// event
	switch x := m.Event.(type) {
	case *StateChange_Creation:
		b.EncodeVarint(1<<3 | proto.WireBytes)
		if err := b.EncodeMessage(x.Creation); err != nil {
			return err
		}
	case *StateChange_Deletion:
		b.EncodeVarint(2<<3 | proto.WireBytes)
		if err := b.EncodeMessage(x.Deletion); err != nil {
			return err
		}
	case *StateChange_RootDeletion:
		b.EncodeVarint(3<<3 | proto.WireBytes)
		if err := b.EncodeMessage(x.RootDeletion); err != nil {
			return err
		}
	case nil:
	default:
		return fmt.Errorf("StateChange.Event has unexpected type %T", x)
	}
	return nil
}

func _StateChange_OneofUnmarshaler(msg proto.Message, tag, wire int, b *proto.Buffer) (bool, error) {
	m := msg.(*StateChange)
	switch tag {
	case 1: // event.creation
		if wire != proto.WireBytes {
			return true, proto.ErrInternalBadWireType
		}
		msg := new(Creation)
		err := b.DecodeMessage(msg)
		m.Event = &StateChange_Creation{msg}
		return true, err
	case 2: // event.deletion
		if wire != proto.WireBytes {
			return true, proto.ErrInternalBadWireType
		}
		msg := new(Deletion)
		err := b.DecodeMessage(msg)
		m.Event = &StateChange_Deletion{msg}
		return true, err
	case 3: // event.root_deletion
		if wire != proto.WireBytes {
			return true, proto.ErrInternalBadWireType
		}
		msg := new(RootDeletion)
		err := b.DecodeMessage(msg)
		m.Event = &StateChange_RootDeletion{msg}
		return true, err
	default:
		return false, nil
	}
}

func _StateChange_OneofSizer(msg proto.Message) (n int) {
	m := msg.(*StateChange)
	// event
	switch x := m.Event.(type) {
	case *StateChange_Creation:
		s := proto.Size(x.Creation)
		n += proto.SizeVarint(1<<3 | proto.WireBytes)
		n += proto.SizeVarint(uint64(s))
		n += s
	case *StateChange_Deletion:
		s := proto.Size(x.Deletion)
		n += proto.SizeVarint(2<<3 | proto.WireBytes)
		n += proto.SizeVarint(uint64(s))
		n += s
	case *StateChange_RootDeletion:
		s := proto.Size(x.RootDeletion)
		n += proto.SizeVarint(3<<3 | proto.WireBytes)
		n += proto.SizeVarint(uint64(s))
		n += s
	case nil:
	default:
		panic(fmt.Sprintf("proto: unexpected type %T in oneof", x))
	}
	return n
}

// Creation defines a module that has been validated, built, and stored in
// the primary source of truth.
type Creation struct {
	Module *Module `protobuf:"bytes,1,opt,name=module" json:"module,omitempty"`
}

func (m *Creation) Reset()                    { *m = Creation{} }
func (m *Creation) String() string            { return proto.CompactTextString(m) }
func (*Creation) ProtoMessage()               {}
func (*Creation) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *Creation) GetModule() *Module {
	if m != nil {
		return m.Module
	}
	return nil
}

// Deletion describes a module that has been marked for deletion.
type Deletion struct {
	ModuleId int64 `protobuf:"varint,1,opt,name=module_id,json=moduleId" json:"module_id,omitempty"`
}

func (m *Deletion) Reset()                    { *m = Deletion{} }
func (m *Deletion) String() string            { return proto.CompactTextString(m) }
func (*Deletion) ProtoMessage()               {}
func (*Deletion) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *Deletion) GetModuleId() int64 {
	if m != nil {
		return m.ModuleId
	}
	return 0
}

// RootDeletion describes a collection of modules with the same root name
// that have all been marked for deletion. A root name in this context
// is the name of a module, excluding the tag information.
type RootDeletion struct {
	// The IDs of all modules of the same root name.
	Children []int64 `protobuf:"varint,1,rep,packed,name=children" json:"children,omitempty"`
}

func (m *RootDeletion) Reset()                    { *m = RootDeletion{} }
func (m *RootDeletion) String() string            { return proto.CompactTextString(m) }
func (*RootDeletion) ProtoMessage()               {}
func (*RootDeletion) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *RootDeletion) GetChildren() []int64 {
	if m != nil {
		return m.Children
	}
	return nil
}

func init() {
	proto.RegisterType((*StateChange)(nil), "io.botbin.proto.module.StateChange")
	proto.RegisterType((*Creation)(nil), "io.botbin.proto.module.Creation")
	proto.RegisterType((*Deletion)(nil), "io.botbin.proto.module.Deletion")
	proto.RegisterType((*RootDeletion)(nil), "io.botbin.proto.module.RootDeletion")
}

func init() {
	proto.RegisterFile("gitlab.com/botbin/module_service/schema/change.proto", fileDescriptor1)
}

var fileDescriptor1 = []byte{
	// 298 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0x8c, 0x91, 0x41, 0x4b, 0xc3, 0x30,
	0x14, 0xc7, 0x9d, 0xc3, 0x59, 0xb3, 0x79, 0xc9, 0x41, 0x64, 0x82, 0x8c, 0xaa, 0x28, 0x1e, 0x12,
	0x50, 0xf1, 0x22, 0x78, 0xe8, 0x3c, 0x28, 0xe2, 0xa5, 0xde, 0xbc, 0x8c, 0x34, 0x7d, 0x6b, 0x1f,
	0xb6, 0x4d, 0x49, 0xe3, 0xc0, 0xcf, 0xeb, 0x17, 0xb1, 0x4d, 0xd3, 0x51, 0xd0, 0x89, 0xa7, 0x92,
	0xc7, 0xff, 0xf7, 0x7b, 0xaf, 0xef, 0x91, 0x9b, 0x04, 0x4d, 0x26, 0x22, 0x26, 0x55, 0xce, 0x23,
	0x65, 0x22, 0x2c, 0x78, 0xae, 0xe2, 0x8f, 0x0c, 0x16, 0x15, 0xe8, 0x15, 0x4a, 0xe0, 0x95, 0x4c,
	0x21, 0x17, 0x5c, 0xa6, 0xa2, 0x48, 0x80, 0x95, 0x5a, 0x19, 0x45, 0x0f, 0x50, 0xb1, 0x36, 0xdd,
	0x16, 0x58, 0xcb, 0x4c, 0xef, 0xfe, 0x6b, 0xab, 0x4a, 0x90, 0xb8, 0x44, 0x29, 0x0c, 0x2a, 0xe7,
	0xf0, 0xbf, 0x06, 0x64, 0xfc, 0x6a, 0x84, 0x81, 0xb9, 0x6d, 0x45, 0xef, 0x89, 0x27, 0x35, 0xd8,
	0xc4, 0xe1, 0x60, 0x36, 0xb8, 0x18, 0x5f, 0xcd, 0xd8, 0xef, 0x7d, 0xd9, 0xdc, 0xe5, 0x1e, 0xb7,
	0xc2, 0x35, 0xd3, 0xf0, 0x31, 0x64, 0x60, 0xf9, 0xed, 0xbf, 0xf9, 0x07, 0x97, 0x6b, 0xf8, 0x8e,
	0xa1, 0xcf, 0x64, 0x5f, 0x2b, 0x65, 0x16, 0x6b, 0xc9, 0xd0, 0x4a, 0x4e, 0x37, 0x49, 0xc2, 0x3a,
	0xdc, 0x13, 0x4d, 0x74, 0xef, 0x1d, 0xec, 0x92, 0x1d, 0x58, 0x41, 0x61, 0xfc, 0x80, 0x78, 0xdd,
	0xb4, 0xf4, 0x96, 0x8c, 0x5a, 0xd6, 0xfd, 0xdf, 0xf1, 0x26, 0xf5, 0x8b, 0xfd, 0x84, 0x2e, 0xed,
	0x9f, 0x13, 0xaf, 0x13, 0xd3, 0x23, 0xb2, 0xe7, 0x56, 0x8c, 0xb1, 0xd5, 0x0c, 0x43, 0xaf, 0x2d,
	0x3c, 0xc5, 0xfe, 0x25, 0x99, 0xf4, 0xa7, 0xa2, 0xd3, 0x7a, 0xa5, 0x29, 0x66, 0xb1, 0x86, 0x66,
	0xa5, 0xc3, 0x26, 0xdb, 0xbd, 0x83, 0xb3, 0xb7, 0x93, 0x9f, 0xd7, 0x2b, 0x94, 0xc1, 0xe5, 0x27,
	0x2f, 0xdf, 0x13, 0x5e, 0x5f, 0x10, 0x45, 0x16, 0x8d, 0xec, 0x60, 0xd7, 0xdf, 0x01, 0x00, 0x00,
	0xff, 0xff, 0x05, 0x65, 0xf1, 0x9e, 0x39, 0x02, 0x00, 0x00,
}
