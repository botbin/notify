// Code generated by protoc-gen-go. DO NOT EDIT.
// source: gitlab.com/botbin/builder/schema/result.proto

/*
Package serial is a generated protocol buffer package.

It is generated from these files:
	gitlab.com/botbin/builder/schema/result.proto

It has these top-level messages:
	Result
	Success
	Failure
*/
package serial

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// Result defines a completed module build.
type Result struct {
	Definition      *Definition `protobuf:"bytes,1,opt,name=definition" json:"definition,omitempty"`
	BuildDurationMs int32                              `protobuf:"varint,2,opt,name=build_duration_ms,json=buildDurationMs" json:"build_duration_ms,omitempty"`
	// Types that are valid to be assigned to Payload:
	//	*Result_Success
	//	*Result_Failure
	Payload isResult_Payload `protobuf_oneof:"payload"`
}

func (m *Result) Reset()                    { *m = Result{} }
func (m *Result) String() string            { return proto.CompactTextString(m) }
func (*Result) ProtoMessage()               {}
func (*Result) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

type isResult_Payload interface {
	isResult_Payload()
}

type Result_Success struct {
	Success *Success `protobuf:"bytes,3,opt,name=success,oneof"`
}
type Result_Failure struct {
	Failure *Failure `protobuf:"bytes,4,opt,name=failure,oneof"`
}

func (*Result_Success) isResult_Payload() {}
func (*Result_Failure) isResult_Payload() {}

func (m *Result) GetPayload() isResult_Payload {
	if m != nil {
		return m.Payload
	}
	return nil
}

func (m *Result) GetDefinition() *Definition {
	if m != nil {
		return m.Definition
	}
	return nil
}

func (m *Result) GetBuildDurationMs() int32 {
	if m != nil {
		return m.BuildDurationMs
	}
	return 0
}

func (m *Result) GetSuccess() *Success {
	if x, ok := m.GetPayload().(*Result_Success); ok {
		return x.Success
	}
	return nil
}

func (m *Result) GetFailure() *Failure {
	if x, ok := m.GetPayload().(*Result_Failure); ok {
		return x.Failure
	}
	return nil
}

// XXX_OneofFuncs is for the internal use of the proto package.
func (*Result) XXX_OneofFuncs() (func(msg proto.Message, b *proto.Buffer) error, func(msg proto.Message, tag, wire int, b *proto.Buffer) (bool, error), func(msg proto.Message) (n int), []interface{}) {
	return _Result_OneofMarshaler, _Result_OneofUnmarshaler, _Result_OneofSizer, []interface{}{
		(*Result_Success)(nil),
		(*Result_Failure)(nil),
	}
}

func _Result_OneofMarshaler(msg proto.Message, b *proto.Buffer) error {
	m := msg.(*Result)
	// payload
	switch x := m.Payload.(type) {
	case *Result_Success:
		b.EncodeVarint(3<<3 | proto.WireBytes)
		if err := b.EncodeMessage(x.Success); err != nil {
			return err
		}
	case *Result_Failure:
		b.EncodeVarint(4<<3 | proto.WireBytes)
		if err := b.EncodeMessage(x.Failure); err != nil {
			return err
		}
	case nil:
	default:
		return fmt.Errorf("Result.Payload has unexpected type %T", x)
	}
	return nil
}

func _Result_OneofUnmarshaler(msg proto.Message, tag, wire int, b *proto.Buffer) (bool, error) {
	m := msg.(*Result)
	switch tag {
	case 3: // payload.success
		if wire != proto.WireBytes {
			return true, proto.ErrInternalBadWireType
		}
		msg := new(Success)
		err := b.DecodeMessage(msg)
		m.Payload = &Result_Success{msg}
		return true, err
	case 4: // payload.failure
		if wire != proto.WireBytes {
			return true, proto.ErrInternalBadWireType
		}
		msg := new(Failure)
		err := b.DecodeMessage(msg)
		m.Payload = &Result_Failure{msg}
		return true, err
	default:
		return false, nil
	}
}

func _Result_OneofSizer(msg proto.Message) (n int) {
	m := msg.(*Result)
	// payload
	switch x := m.Payload.(type) {
	case *Result_Success:
		s := proto.Size(x.Success)
		n += proto.SizeVarint(3<<3 | proto.WireBytes)
		n += proto.SizeVarint(uint64(s))
		n += s
	case *Result_Failure:
		s := proto.Size(x.Failure)
		n += proto.SizeVarint(4<<3 | proto.WireBytes)
		n += proto.SizeVarint(uint64(s))
		n += s
	case nil:
	default:
		panic(fmt.Sprintf("proto: unexpected type %T in oneof", x))
	}
	return n
}

// Success details a successful module build.
type Success struct {
	Location *Location `protobuf:"bytes,1,opt,name=location" json:"location,omitempty"`
}

func (m *Success) Reset()                    { *m = Success{} }
func (m *Success) String() string            { return proto.CompactTextString(m) }
func (*Success) ProtoMessage()               {}
func (*Success) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *Success) GetLocation() *Location {
	if m != nil {
		return m.Location
	}
	return nil
}

// Failure details a failed module build.
type Failure struct {
	Reason string `protobuf:"bytes,2,opt,name=reason" json:"reason,omitempty"`
}

func (m *Failure) Reset()                    { *m = Failure{} }
func (m *Failure) String() string            { return proto.CompactTextString(m) }
func (*Failure) ProtoMessage()               {}
func (*Failure) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *Failure) GetReason() string {
	if m != nil {
		return m.Reason
	}
	return ""
}

func init() {
	proto.RegisterType((*Result)(nil), "io.botbin.proto.module.build.Result")
	proto.RegisterType((*Success)(nil), "io.botbin.proto.module.build.Success")
	proto.RegisterType((*Failure)(nil), "io.botbin.proto.module.build.Failure")
}

func init() { proto.RegisterFile("gitlab.com/botbin/builder/schema/result.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 309 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0x8c, 0x91, 0x4f, 0x4b, 0xc3, 0x30,
	0x18, 0xc6, 0xdd, 0xd4, 0xd5, 0xc5, 0x83, 0xd8, 0x83, 0x14, 0xf1, 0x30, 0x2b, 0x03, 0x11, 0x4c,
	0x40, 0x8f, 0x7a, 0x71, 0x0c, 0xf5, 0xa0, 0x97, 0x78, 0xf3, 0x32, 0xd2, 0x34, 0x9d, 0x2f, 0xa6,
	0x4b, 0xc9, 0x1f, 0x61, 0x1f, 0xc1, 0x6f, 0x6d, 0x9b, 0x64, 0x43, 0x18, 0x13, 0x8f, 0xcd, 0xfb,
	0x3c, 0xbf, 0x5f, 0xf3, 0x06, 0x5d, 0xcf, 0xc1, 0x4a, 0x56, 0x60, 0xae, 0x6a, 0x52, 0x28, 0x5b,
	0xc0, 0x82, 0x14, 0x0e, 0x64, 0x29, 0x34, 0x31, 0xfc, 0x43, 0xd4, 0x8c, 0x68, 0x61, 0x9c, 0xb4,
	0xb8, 0xd1, 0xca, 0xaa, 0xf4, 0x0c, 0x14, 0x0e, 0xb1, 0x70, 0x80, 0x6b, 0x55, 0x3a, 0x29, 0xb0,
	0xef, 0x9c, 0xde, 0x6d, 0xc2, 0xc2, 0x7c, 0x66, 0x84, 0xfe, 0x02, 0x2e, 0x56, 0x4c, 0xd3, 0x08,
	0x0e, 0x15, 0x70, 0x66, 0x41, 0x45, 0x52, 0xfe, 0xdd, 0x47, 0x03, 0xea, 0x5d, 0xe9, 0x04, 0xa1,
	0x52, 0x54, 0xb0, 0x80, 0x6e, 0x9c, 0xf5, 0x46, 0xbd, 0xcb, 0xc3, 0x9b, 0x1c, 0x6f, 0x51, 0x4f,
	0xd7, 0x49, 0xfa, 0xab, 0x95, 0x5e, 0xa1, 0x63, 0xff, 0x53, 0xb3, 0xd2, 0x69, 0xaf, 0x99, 0xd5,
	0x26, 0xeb, 0xb7, 0xa8, 0x7d, 0x7a, 0xe4, 0x07, 0xd3, 0x78, 0xfe, 0x6a, 0xd2, 0x07, 0x94, 0x18,
	0xc7, 0xb9, 0x30, 0x26, 0xdb, 0xf5, 0xb2, 0x31, 0xfe, 0xeb, 0x9e, 0xf8, 0x2d, 0x84, 0x9f, 0x77,
	0xe8, 0xaa, 0xd7, 0x21, 0x2a, 0x06, 0xd2, 0x69, 0x91, 0xed, 0xfd, 0x07, 0xf1, 0x18, 0xc2, 0x1d,
	0x22, 0xf6, 0x26, 0x43, 0x94, 0x34, 0x6c, 0x29, 0x15, 0x2b, 0xf3, 0x27, 0x94, 0x44, 0x47, 0x7a,
	0x8f, 0x0e, 0xa4, 0x0a, 0x8b, 0x8a, 0x9b, 0x18, 0x6d, 0x23, 0xbf, 0xc4, 0x1c, 0x5d, 0x37, 0xf2,
	0x73, 0x94, 0x44, 0x53, 0x7a, 0x82, 0x06, 0x5a, 0x30, 0xd3, 0x62, 0xba, 0x2d, 0x0c, 0x69, 0xfc,
	0x9a, 0x8c, 0xdf, 0x2f, 0x36, 0x9f, 0x6d, 0xa1, 0x2c, 0x54, 0x4b, 0xd2, 0x7c, 0xce, 0x49, 0xfb,
	0x74, 0xc0, 0x64, 0x31, 0xf0, 0xaa, 0xdb, 0x9f, 0x00, 0x00, 0x00, 0xff, 0xff, 0x20, 0x0f, 0xae,
	0x08, 0x31, 0x02, 0x00, 0x00,
}
