package event

import (
	"encoding/json"

	"go.uber.org/zap"
)

// Notification contains sanitized information about an event that
// a user is interesting in knowing. This data is assumed to go
// straight to a user, so ensure that an appropriate level of
// detail is shown.
type Notification struct {
	// Timestamp should indicate when the event occurred or default
	// to the time this notification was created.
	Timestamp int64 `json:"timestamp"`

	// TODO: Use the user ID as the value of Recipient.

	// The username of the user who should receive the notification.
	// This value does not appear in the result of the String() method.
	Recipient string `json:"-"`

	// Type indicates what type of information the notification contains.
	Type NotificationType `json:"type"`

	// Title should hold a brief description of the event.
	Title string `json:"title"`

	// Message should contain a complete picture of the event.
	Message string `json:"message"`
}

// String returns JSON string representation of the notification.
func (n *Notification) String() string {
	buf, err := json.Marshal(n)
	if err != nil {
		zap.S().Errorw("failed to marshal notification",
			"notification", n,
			"error", err,
		)
		return ""
	}
	return string(buf)
}

// NotificationType specifies what type of information a Notification contains.
type NotificationType int

const (
	// BuildStatus indicates the result of a module build.
	BuildStatus NotificationType = iota + 1
)
