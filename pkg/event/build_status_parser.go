package event

import (
	"fmt"
	"time"

	"github.com/golang/protobuf/proto"
	"gitlab.com/botbin/notify/pkg/serial"
)

// BuildStatusParser turns build status messages into notifications.
type BuildStatusParser struct {
}

// Parse creates a notification from a build status.
//
// It will only create notifications for messages that belong to the
// build status context. The context consists of:
//
//		Build failures
//		Module creations
//
// If a message does not belong to one of those contexts, this method will
// return a nil value.
func (bsp BuildStatusParser) Parse(message proto.Message) *Notification {
	switch m := message.(type) {
	case *serial.Result:
		return bsp.handleBuildResult(m)

	case *serial.StateChange:
		return bsp.handleStateChange(m)

	default:
		return nil
	}
}

func (bsp BuildStatusParser) handleBuildResult(result *serial.Result) *Notification {
	if result.GetFailure() == nil {
		return nil
	}

	// TODO: Determine if the failure reason is safe to put in the message.

	return &Notification{
		Timestamp: time.Now().UTC().Unix(),
		Recipient: result.GetDefinition().GetMeta().GetAuthor(),
		Type:      BuildStatus,
		Title:     "Module Creation Failed",
		Message: fmt.Sprintf("%s could not be created due to either an API or syntax error",
			result.GetDefinition().GetMeta().GetFqn()),
	}
}

func (bsp BuildStatusParser) handleStateChange(change *serial.StateChange) *Notification {
	module := change.GetCreation().GetModule()
	if module == nil {
		return nil
	}

	return &Notification{
		Timestamp: time.Now().UTC().Unix(),
		Recipient: module.GetDefinition().GetMeta().GetAuthor(),
		Type:      BuildStatus,
		Title:     "Module Successfully Created",
		Message: fmt.Sprintf("%s was successfully created and can now be used by bots",
			module.GetDefinition().GetMeta().GetFqn()),
	}
}
