package event

import "github.com/golang/protobuf/proto"

// A Parser extracts relevant data from messages and turns it into
// user-friendly notifications.
type Parser interface {
	Parse(proto.Message) *Notification
}
