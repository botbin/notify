package event_test

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/notify/pkg/event"
	"gitlab.com/botbin/notify/pkg/serial"
)

// BuildStatusParserTestSuite runs tests for BuildStatusParser.
type BuildStatusParserTestSuite struct {
	suite.Suite
	parser event.Parser
}

func (bs *BuildStatusParserTestSuite) SetupSuite() {
	bs.parser = event.BuildStatusParser{}
}

func (bs *BuildStatusParserTestSuite) TestParse_Failure() {
	msg := &serial.Result{
		Definition: bs.getDefinition(),
		Payload: &serial.Result_Failure{
			Failure: &serial.Failure{
				Reason: "test",
			},
		},
	}

	notification := bs.parser.Parse(msg)
	bs.NotNil(notification)
	bs.assertNoMissingFields(notification)
}

func (bs *BuildStatusParserTestSuite) TestParse_Creation() {
	msg := &serial.StateChange{
		Event: &serial.StateChange_Creation{
			Creation: &serial.Creation{
				Module: &serial.Module{
					Definition: bs.getDefinition(),
				},
			},
		},
	}

	notification := bs.parser.Parse(msg)
	bs.NotNil(notification)
	bs.assertNoMissingFields(notification)
}

func (bs *BuildStatusParserTestSuite) TestParse_Unrecognized() {
	msg := &serial.Result{
		Payload: &serial.Result_Success{
			Success: new(serial.Success),
		},
	}
	bs.Nil(bs.parser.Parse(msg))
}

func (bs *BuildStatusParserTestSuite) getDefinition() *serial.Definition {
	return &serial.Definition{
		Meta: &serial.Metadata{
			Fqn:         "test/test:0.1.0",
			Author:      "test",
			Name:        "test",
			Tag:         "test",
			Description: "test",
		},
		Spec: &serial.Spec{
			Language: "js",
			Code:     "test",
		},
	}
}

func (bs *BuildStatusParserTestSuite) assertNoMissingFields(n *event.Notification) {
	bs.NotEmpty(n.Timestamp)
	bs.NotEmpty(n.Recipient)
	bs.NotEmpty(n.Type)
	bs.NotEmpty(n.Title)
	bs.NotEmpty(n.Message)
}

func TestBuildStatusParser(t *testing.T) {
	suite.Run(t, new(BuildStatusParserTestSuite))
}
