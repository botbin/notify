package event_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/botbin/notify/pkg/event"
)

func TestNotification_String(t *testing.T) {
	notification := &event.Notification{
		Timestamp: time.Now().UTC().Unix(),
		Recipient: "test",
		Type:      event.BuildStatus,
		Title:     "test",
		Message:   "test",
	}

	assert.NotEmpty(t, notification.String())
}
