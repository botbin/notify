package pipe_test

import (
	"testing"
	"time"

	"gitlab.com/botbin/notify/pkg/event"
	"gitlab.com/botbin/notify/pkg/pipe"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	mmocks "gitlab.com/botbin/notify/pkg/message/mocks"
	pmocks "gitlab.com/botbin/notify/pkg/pipe/mocks"
)

// CoordinatorTestSuite handles tests for the default Coordinator.
type CoorindatorTestSuite struct {
	suite.Suite
}

// Ensures that calling start with no publisher defined results
// in an error.
func (cs *CoorindatorTestSuite) TestStart_NoPublisher() {
	diny := pipe.NewCoordinator()
	cs.NotNil(diny.Start())
}

// Ensures that calling start with a publisher defined will start
// the coordination process.
func (cs *CoorindatorTestSuite) TestStart_WithPublisher() {
	diny := pipe.NewCoordinator()
	diny.Use(new(mmocks.Publisher))
	go func() {
		time.Sleep(500 * time.Millisecond)
		diny.Stop()
	}()
	cs.Nil(diny.Start())
}

// Ensures that calling accept does not throw an error if the
// coordinator has not been started.
func (cs *CoorindatorTestSuite) TestAccept_NotStarted() {
	diny := pipe.NewCoordinator()
	cs.Nil(diny.Accept(new(pmocks.EventHandler)))
}

// Ensures that calling accept after the coordinator has been
// started will cause an error.
func (cs *CoorindatorTestSuite) TestAccept_Started() {
	diny := pipe.NewCoordinator()
	diny.Use(new(mmocks.Publisher))
	go func() {
		diny.Start()
		time.Sleep(500 * time.Millisecond)
	}()
	time.Sleep(100 * time.Millisecond)
	cs.NotNil(diny.Start()) // make sure it started
	cs.NotNil(diny.Accept(new(pmocks.EventHandler)))
}

// Ensure that, once started, the coordinator will route handler
// output to the defined publisher.
func (cs *CoorindatorTestSuite) TestRouting() {
	pub := new(mmocks.Publisher)
	pub.On("Publish", mock.Anything).Return(nil)

	handler := new(pmocks.EventHandler)
	handled := make(chan *event.Notification)
	var proper <-chan *event.Notification = handled
	handler.On("Stream").Return(proper).
			On("Close").Return(nil)

	diny := pipe.NewCoordinator()
	diny.Use(pub)
	diny.Accept(handler)

	// TODO: more stuff
	go diny.Start()
	handled <- new(event.Notification)
	diny.Stop()

	pub.AssertExpectations(cs.T())
	handler.AssertExpectations(cs.T())
}

func TestCoordinator(t *testing.T) {
	suite.Run(t, new(CoorindatorTestSuite))
}
