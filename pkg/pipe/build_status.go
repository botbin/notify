package pipe

import (
	"sync"

	"github.com/golang/protobuf/proto"
	"gitlab.com/botbin/notify/pkg/event"
	"gitlab.com/botbin/notify/pkg/message"
	"gitlab.com/botbin/notify/pkg/serial"
)

type buildStatusHandler struct {
	brokers          []string
	creationConsumer message.Consumer
	failureConsumer  message.Consumer

	parser         event.Parser
	notifications  chan *event.Notification
	doOpenPipeline sync.Once
}

// NewBuildStatusHandler creates an EventHandler that processes events
// related to the status of module builds.
func NewBuildStatusHandler(brokers []string) EventHandler {
	handler := &buildStatusHandler{
		brokers:       brokers,
		notifications: make(chan *event.Notification),
	}

	handler.initCreationConsumer()
	handler.initFailureConsumer()
	handler.initParser()

	return handler
}

func (bsh *buildStatusHandler) Close() error {
	err0 := bsh.creationConsumer.Close()
	err1 := bsh.failureConsumer.Close()

	if err0 != nil {
		return err0
	}
	return err1
}

func (bsh *buildStatusHandler) Stream() <-chan *event.Notification {
	bsh.doOpenPipeline.Do(func() {
		go bsh.pipe(bsh.failureConsumer)
		go bsh.pipe(bsh.creationConsumer)
	})
	return bsh.notifications
}

func (bsh *buildStatusHandler) pipe(consumer message.Consumer) {
	for message := range consumer.Consume() {
		notification := bsh.parser.Parse(message)
		if notification != nil {
			bsh.notifications <- notification
		}
	}
}

func (bsh *buildStatusHandler) initCreationConsumer() {
	converter := func(data []byte) (proto.Message, error) {
		change := new(serial.StateChange)
		err := proto.Unmarshal(data, change)
		return change, err
	}

	bsh.creationConsumer = message.NewConsumer(message.ConsumerConfig{
		Brokers: bsh.brokers,
		Topic:   "module.state.change",
	}, converter)
}

func (bsh *buildStatusHandler) initFailureConsumer() {
	converter := func(data []byte) (proto.Message, error) {
		result := new(serial.Result)
		err := proto.Unmarshal(data, result)
		return result, err
	}

	bsh.failureConsumer = message.NewConsumer(message.ConsumerConfig{
		Brokers: bsh.brokers,
		Topic:   "module.build.result",
	}, converter)
}

func (bsh *buildStatusHandler) initParser() {
	bsh.parser = event.BuildStatusParser{}
}
