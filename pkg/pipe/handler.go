package pipe

import "gitlab.com/botbin/notify/pkg/event"

// An EventHandler processes events to produce a stream of notifications.
type EventHandler interface {
	Stream() <-chan *event.Notification
	Close() error
}
