package pipe

import (
	"errors"

	"gitlab.com/botbin/notify/pkg/message"
	"go.uber.org/zap"
)

// A Coordinator routes the results of a group of event handlers to
// a single publisher instance.
type Coordinator interface {
	Use(message.Publisher)
	Accept(EventHandler) error
	Start() error
	Stop()
}

type coordinator struct {
	publisher message.Publisher
	handlers  []EventHandler
	started   bool
	done      chan bool
}

// NewCoordinator configures a Coordinator instance.
func NewCoordinator() Coordinator {
	return &coordinator{done: make(chan bool)}
}

// Use sets the publisher that will process event handler results.
// Only one publisher may be used; thus, repeated calls will overwrite
// any existing publisher.
func (c *coordinator) Use(pub message.Publisher) {
	c.publisher = pub
}

// Accept adds an event handler to the coordinated group.
//
// Multiple handlers may be added and their output will all go to the
// same publisher.
//
// This method will return an error if the coordinator has already
// started.
func (c *coordinator) Accept(handler EventHandler) error {
	if c.started {
		return errors.New("can't add handler after coordinator has started")
	}
	c.handlers = append(c.handlers, handler)
	return nil
}

// Start begins routing the results of all handlers to the publisher.
// This method will fail if a publisher has not been set or the coordinator
// is already running. It will block until Stop() is called.
func (c *coordinator) Start() error {
	if c.started {
		return errors.New("cannot start coordinator that is already running")
	}
	if c.publisher == nil {
		return errors.New("cannot start coordinator without publisher")
	}

	c.started = true
	for _, handler := range c.handlers {
		go c.publishNotifications(handler)
	}
	<-c.done

	return nil
}

func (c *coordinator) publishNotifications(handler EventHandler) {
	for notification := range handler.Stream() {
		err := c.publisher.Publish(notification)
		if err != nil {
			zap.S().Errorw("failed to publish notification",
				"notification", notification,
				"error", err,
			)
		}
	}
}

// Stop closes all handlers and ends the coordination process.
// This will cause Start() to let go of its lock.
func (c *coordinator) Stop() {
	if !c.started {
		return
	}

	for _, handler := range c.handlers {
		if err := handler.Close(); err != nil {
			zap.S().Errorw("encountered error when closing handler",
				"error", err,
			)
		}
	}

	c.done <- true
	c.started = false
}
