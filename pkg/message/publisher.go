package message

import (
	"os"

	"github.com/go-redis/redis"
	"gitlab.com/botbin/notify/pkg/event"
	"go.uber.org/zap"
)

// Publisher types send notifications to users.
type Publisher interface {
	Publish(*event.Notification) error
}

// redisPublisher publishes notifications to Redis.
type redisPublisher struct {
	client *redis.Client
}

// NewPublisher configures a Publisher that sends notifications to Redis.
func NewPublisher() (Publisher, error) {
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")
	if port == "" {
		port = "6379"
	}

	publisher := &redisPublisher{}
	return publisher, publisher.connect(host, port)
}

func (rp *redisPublisher) connect(host, port string) error {
	rp.client = redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Password: "",
		DB:       0,
	})

	_, err := rp.client.Ping().Result()
	if err != nil {
		zap.S().Errorw("could not connect to redis",
			"host", host,
			"port", port,
		)
	} else {
		zap.S().Infow("connected to redis",
			"host", host,
			"port", port,
		)
	}
	return err
}

// Publish sends a notification to Redis.
//
// The notification is published to a channel and also persisted
// in a sorted set. This method allows clients to receive live updates
// but also for offline users to be notified later.
func (rp *redisPublisher) Publish(notification *event.Notification) error {
	_, err := rp.client.Pipelined(func(pipe redis.Pipeliner) error {
		payload := notification.String()

		pipe.ZAdd("notifications:"+notification.Recipient, redis.Z{
			Score:  float64(notification.Timestamp),
			Member: payload,
		})

		pipe.Publish("notification_stream:"+notification.Recipient, payload)
		return nil
	})
	return err
}
