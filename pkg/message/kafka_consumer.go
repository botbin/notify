package message

import (
	"context"
	"io"
	"sync"

	"github.com/golang/protobuf/proto"
	kafka "github.com/segmentio/kafka-go"
	"go.uber.org/zap"
)

// A Converter converts a raw message to a protobuf message.
type Converter func([]byte) (proto.Message, error)

type kafkaConsumer struct {
	config         ConsumerConfig
	reader         *kafka.Reader
	messages       chan proto.Message
	startConsuming sync.Once
	convert        Converter
}

// NewConsumer creates a Consumer that gets messages from Kafka.
func NewConsumer(config ConsumerConfig, converter Converter) Consumer {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: config.Brokers,
		Topic:   config.Topic,
		GroupID: "notify",
	})

	return &kafkaConsumer{
		config:   config,
		reader:   reader,
		messages: make(chan proto.Message),
		convert:  converter,
	}
}

func (kc *kafkaConsumer) Consume() <-chan proto.Message {
	kc.startConsuming.Do(func() {
		go kc.consume()
	})
	return kc.messages
}

func (kc *kafkaConsumer) consume() {
	for {
		message, err := kc.reader.ReadMessage(context.Background())
		if err == io.EOF {
			return
		} else if err != nil {
			zap.S().Error(err)
			continue
		}

		converted, err := kc.convert(message.Value)
		if err != nil {
			zap.S().Errorw("failed to parse message",
				"topic", kc.config.Topic,
				"partition", message.Partition,
				"offset", message.Offset,
				"message", proto.MarshalTextString(converted),
				"error", err,
			)
		} else {
			kc.messages <- converted
		}
	}
}

func (kc *kafkaConsumer) Close() error {
	return kc.reader.Close()
}
