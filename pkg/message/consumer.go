package message

import (
	"github.com/golang/protobuf/proto"
)

// A Consumer reads messages from some source.
type Consumer interface {
	Consume() <-chan proto.Message
	Close() error
}

// ConsumerConfig defines how a consumer should function.
type ConsumerConfig struct {
	Brokers []string
	Topic   string
}
